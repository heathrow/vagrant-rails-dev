# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.hostname = "rails-dev"

  config.vm.box = "precise64"
  config.vm.box_url = "http://files.vagrantup.com/precise64.box"

  config.vm.network "private_network", ip: "33.33.33.10"

  config.vm.synced_folder "./data", "/vagrant_data"

  config.vm.provider "virtualbox" do |vb|
    vb.customize ["modifyvm", :id, "--memory", "1024"]
  end

  config.omnibus.chef_version = :latest

  config.vm.provision "chef_solo" do |chef|
    chef.add_recipe "apt"
    chef.add_recipe "build-essential"
    chef.add_recipe "git"
    chef.add_recipe "ruby_build"
    chef.add_recipe "rbenv::user"
    chef.add_recipe "mysql::client"
    chef.add_recipe "mysql::server"
    chef.add_recipe "redisio::install"
    chef.add_recipe "redisio::enable"

    ruby_version = "2.1.2"
    chef.json = {
      rbenv: {
        user_installs: [{
          user: "vagrant",
          rubies: [ruby_version],
          global: ruby_version,
          gems: {
            ruby_version => [{
              name: "bundler",
              options: "--no-ri --no-rdoc"
            }]
          }
        }],
      },
      mysql: {
        server_root_password:   "password",
        server_repl_password:   "password",
        server_debian_password: "password"
      }
    }
  end
end
