# Vagrant Rails Dev

## これは何?
Railsアプリケーションを動かすための最低限の環境を、Vagrant+Chefでサクッとつくる

## インストールされるもの
- Ruby(rbenv)
- MySQL
- Redis
- Git
